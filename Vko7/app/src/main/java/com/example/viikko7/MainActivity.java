package com.example.viikko7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView text;
    EditText input;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.textView2);
        input = findViewById(R.id.editText);



    }



    public void changeText(View v) {
        text.setText("Hello World!");
    }

    public void editText(View v) {

        text.setText(input.getText().toString());
    }

    //Tehtävä 4
    public void readText(View v, int keyCode, KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            text.setText(input.getText().toString());
        }




    }
}
